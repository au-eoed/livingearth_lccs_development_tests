# Living Earth LCCS Development Tests

| | |
|-|-|
|__Disclaimer__| This repository is *in development*, use at your own risk |
|__License__| The Apache 2.0 license applies to this open source code. |


### About
This repository includes a collection of Jupyter notebooks and scrips for use during development of the [LivingEarth LCCS](https://bitbucket.org/au-eoed/livingearth_lccs/src/master/) system.

*NOTE: Likely to change and break*
