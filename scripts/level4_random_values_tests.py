#!/usr/bin/env python
"""
Script to randomly select from all possible input values for level 4 input
layers and calculate LCCS level 4 classes and codes.

Dan Clewley - 2019-05-18

# Run 20 instances in parallel
parallel ./level4_random_values_tests.py level4_classes_random_run_{}.csv --num_picks 50000 --max_runs 1000 ::: 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20

# Sort, keeping only unique values and save to a single file.
sort -u level4_classes_random_run* > all_level4_classes_multiple_runs.csv

# Chris and Pete edited to print variable importance for level 4 paper
# python level4_random_values_tests.py ../test.csv --max_runs 100 --num_picks 1000 --var_importance ../test_var_importance.csv --level3_class

Or to run each level3 class in parallel:

parallel ./level4_random_values_tests.py --level3_class {} --var_importance lccs_level3_{}_var_importance.csv lccs_level3_{}_values.csv --max_runs 1000000 ::: 111 112 123 124 215 216 227 228
"""

import argparse
import numpy
import xarray

numpy.warnings.filterwarnings('ignore')

import sys
sys.path.append("../../livingearth_lccs")

from le_lccs.le_classification import lccs_l4

# Print output variable names
PRINT_OUTPUT = False

# Number of items to pick for each later (i.e., number to calculate at once).
NUM_PICKS = 10000

# Maximum number of runs
# Total number of random combinations will be NUM_PICKS x MAX_RUNS.
MAX_RUNS = 10000

def write_class_description_to_dict(values_dict, all_class_codes_descriptions={},
                                    in_classification_data=None,
                                    num_picks=NUM_PICKS, sum_var_import_data=None, sum_var_import_uniq_data=None,
                                    class_code_lut={}):
    """
    Calculate level 4 class for values dictionary
    """
    if in_classification_data is None:
        dataset_dict = {}
        for layer_name, possible_values in values_dict.items():
            dataset_dict[layer_name] = ("object",
                                        numpy.random.choice(possible_values, num_picks))
        in_classification_data = xarray.Dataset(dataset_dict,
                                                coords={"object" : numpy.arange(0, num_picks)})
    else:
        for layer_name, possible_values in values_dict.items():
            in_classification_data[layer_name].values = \
                numpy.random.choice(possible_values, num_picks)

    classification_array = lccs_l4.classify_lccs_level4(in_classification_data)

    # Convert to binary mask where the output is a valid level 4 class.
    # FIXME: Not sure how this handles data which were 0 in the input.
    var_import_class_data = (classification_array > 0)

    # Add to overall sum for valid classes
    sum_var_import_data = sum_var_import_data + var_import_class_data.sum()

    # Get codes for each class
    class_codes = lccs_l4.get_lccs_level4_code(classification_array)

    for code_idx in range(len(class_codes)):
        code = class_codes[code_idx]
        if code not in class_code_lut:
            sum_var_import_uniq_data = sum_var_import_uniq_data + var_import_class_data.isel(object=code_idx)
            class_code_lut[code] = True

    class_descriptions = lccs_l4.get_lccs_level4_description(classification_array)


    # If class code isn't already in array save to output dictionary
    new_classes = 0
    for class_code, class_description in zip(class_codes, class_descriptions):
        try:
            all_class_codes_descriptions[class_code]
        except KeyError:
            # Save out to dictionary
            all_class_codes_descriptions[class_code] = class_description
            new_classes += 1
            if PRINT_OUTPUT:
                print("{} : {}".format(class_code, class_description))

    if new_classes == 0:
        print("No new classes found")
        raise StopIteration("No new classes found")
    else:
        print(" Found {} new classes ({} %)".format(new_classes, 100*new_classes/class_codes.size))

    return in_classification_data, sum_var_import_data, sum_var_import_uniq_data, class_code_lut

if __name__ == "__main__":

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("outputcsv", type=str,
                        help="Output CSV file to save to.")
    parser.add_argument("--max_runs", type=int, default=MAX_RUNS,
                        help="Maximum number of runs.")
    parser.add_argument("--num_picks", type=int, default=NUM_PICKS,
                        help="Number of picks per run.")
    parser.add_argument("--level3_class", type=int, default=None,
                        help="Specify a level3 class to run for. Default is to run for all.")
    parser.add_argument("--previous_classes", default=None,
                        help="Output from previous run to us as a starting point.")
    parser.add_argument("--var_importance", default=None,
                        help="CSV file to save variable importance to")
    args = parser.parse_args()

    out_dict_file = args.outputcsv

    # Set up a dictionary of each variable to pass to create_var_list
    input_layer_vars_dict = {}
    all_class_codes_descriptions = {}

    # If a CSV file has been provided with outputs from a previous
    # run load this.
    if args.previous_classes:
        with open(args.previous_classes, "r") as prev_f:
            for line in prev_f:
                code = line.split(",")[0]
                # Description might have commas in to take all values
                # after the code and join back together.
                # strip off new line
                description = ",".join(line.split(",")[1:]).strip()
                # Remove quotes from description if they have been used
                description = description.lstrip('"').rstrip('"')
                # Add to dictionary
                all_class_codes_descriptions[code] = description

    total_outputs = 1

    try:
        for layer_class in lccs_l4.ALL_L4_LAYER_CLASSES_LCCS:
            # For all classes except level3 also store 0 (no data).
            if layer_class().input_layer_name != "level3":
                input_layer_vars_dict[layer_class().input_layer_name] = numpy.array([numpy.nan, 0] + layer_class().get_valid_input_values())
                total_outputs = total_outputs * input_layer_vars_dict[layer_class().input_layer_name].size
            elif args.level3_class is not None:
                input_layer_vars_dict[layer_class().input_layer_name] = numpy.array([args.level3_class])
            else:
                input_layer_vars_dict[layer_class().input_layer_name] = numpy.array(layer_class().get_valid_input_values())
                total_outputs = total_outputs * input_layer_vars_dict[layer_class().input_layer_name].size


        # Set up variable importance
        sum_var_import_dict = {'level3': 0.0, 'lifeform_veg_cat_l4a': 0.0, 'mlifefrm_veg_cat_l4m': 0.0,
                               'canopyco_veg_cat_l4d': 0.0, 'canopyht_veg_cat_l4d': 0.0, 'waterday_agr_cat_l4a': 0.0,
                               'watersea_veg_cat_l4a': 0.0, 'leaftype_veg_cat_l4a': 0.0, 'phenolog_veg_cat_l4a': 0.0,
                               'mphenlog_veg_cat_l4m': 0.0, 'spatdist_veg_cat_l4a': 0.0, 'strat2nd_veg_cat_l4a': 0.0,
                               'lifef2nd_veg_cat_l4a': 0.0, 'cover2nd_veg_cat_l4d': 0.0, 'heigh2nd_veg_cat_l4d': 0.0,
                               'spatsize_agr_cat_l4a': 0.0, 'cropcomb_agr_cat_l4a': 0.0, 'croplfc2_agr_cat_l4a': 0.0,
                               'croplfc3_agr_cat_l4a': 0.0, 'cropseqt_agr_cat_l4a': 0.0, 'cropseqa_agr_cat_l4a': 0.0,
                               'watersup_agr_cat_l4a': 0.0, 'timefact_agr_cat_l4a': 0.0, 'waterstt_wat_cat_l4a': 0.0,
                               'waterper_wat_cat_l4d': 0.0, 'inttidal_wat_cat_l4a': 0.0, 'snowcper_wat_cat_l4d': 0.0,
                               'waterdpt_wat_cat_l4a': 0.0, 'mwatrmvt_wat_cat_l4m': 0.0, 'wsedload_wat_cat_l4a': 0.0,
                               'msubstrt_wat_cat_l4m': 0.0, 'baresurf_phy_cat_l4a': 0.0, 'mbaremat_phy_cat_l4m': 0.0,
                               'mustones_phy_cat_l4m': 0.0, 'mhardpan_phy_cat_l4m': 0.0, 'macropat_phy_cat_l4a': 0.0,
                               'artisurf_urb_cat_l4a': 0.0, 'mtclinea_urb_cat_l4m': 0.0, 'mnolinea_urb_cat_l4m': 0.0,
                               'martdens_urb_cat_l4d': 0.0, 'mnobuilt_urb_cat_l4m': 0.0, 'urbanveg_urb_cat_l4a': 0.0}

        sum_var_import_data = xarray.Dataset(sum_var_import_dict, coords={"object": numpy.arange(0, 1)})
        sum_var_import_uniq_data = xarray.Dataset(sum_var_import_dict, coords={"object": numpy.arange(0, 1)})


        # Calculate class code for all input variables and save to a dictionary
        in_classification_data = None
        class_code_lut = {}


        # print("Total of {} layers".format(len(lccs_l4.ALL_L4_LAYER_CLASSES_LCCS)))
        # print("Maximum number of input combinations is {:,}".format(total_outputs))
        print("Max runs {:,}".format(args.max_runs))
        print("Num picks {:,}".format(args.num_picks))
        print("Testing with {:,} random combinations".format(args.max_runs * args.num_picks))


        for i in range(1, args.max_runs+1):
            print("Run {}/{}".format(i, args.max_runs))
            try:
                in_classification_data, sum_var_import_data, sum_var_import_uniq_data, class_code_lut = \
                    write_class_description_to_dict(input_layer_vars_dict,
                                                    all_class_codes_descriptions,
                                                    in_classification_data,
                                                    num_picks=args.num_picks, sum_var_import_data=sum_var_import_data,
                                                    sum_var_import_uniq_data=sum_var_import_uniq_data,
                                                    class_code_lut=class_code_lut)
            except StopIteration as e:
                print(e)
                break


        n_unq = len(class_code_lut)
        print("{} unique classes found".format(n_unq))

        sum_var_import_uniq_data = (sum_var_import_uniq_data / n_unq) * 100

        if args.var_importance is not None:
            out_vf = open(args.var_importance, "w")
            out_vf.write('Variable,Importance\n')
            for var in sum_var_import_uniq_data.data_vars:
                out_vf.write('{},{:.4}\n'.format(var, sum_var_import_uniq_data[var].values))
            out_vf.close()

    except KeyboardInterrupt:
        print("Interrupt - saving classes calculated")
    finally:
        # Write out dictionary to text file.
        out_f = open(out_dict_file, "w")
        all_class_codes = list(all_class_codes_descriptions.keys())
        all_class_codes.sort()
        out_f.write('Code,Description\n')
        for code in all_class_codes:
            out_f.write('{},"{}"\n'.format(code, all_class_codes_descriptions[code]))
        out_f.close()

    print("Saved to: {}".format(out_dict_file))

