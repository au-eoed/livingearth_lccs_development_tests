#!/usr/bin/env python
# An environmental layers testing framework for the FAO land cover classification system
# Carole Planque - 2019-09-19

import argparse
import numpy
import xarray
import yaml
import rasterio
from matplotlib import pyplot
import datacube
from datacube.storage import masking
from datacube.helpers import write_geotiff
from datacube.utils.geometry import CRS

# must specific lccs_dev environment for MADs and mangrove datasets
# dc = datacube.Datacube(env='lccs_dev', app="level4")
dc = datacube.Datacube(app="level4")

#import le_lccs modules
import sys
import copy
sys.path.append('../../../livingearth_lccs')
from le_lccs.le_ingest import gridded_ingest
from le_lccs.le_classification import lccs_l3
from le_lccs.le_classification import lccs_l4

def run_l4_classification(config_file):
    """
    Run Level 4 classification with extent being defined by a config file
    """

    # Read in config file
    print("Reading in config file")
    with open(config_file, "r") as f:
        config = yaml.safe_load(f)

    # Set up dictionary to define size parameters
    target_size = {}
    target_size["target_min_x"] = config["extent"]["min_x"]
    target_size["target_max_x"] = config["extent"]["max_x"]
    target_size["target_min_y"] = config["extent"]["min_y"]
    target_size["target_max_y"] = config["extent"]["max_y"]

    target_size["target_pixel_size_x"] = config["resolution"][0]
    target_size["target_pixel_size_y"] = config["resolution"][1]

    target_size["target_crs"] = config["crs"]

    x = (target_size["target_min_x"], target_size["target_max_x"])
    y = (target_size["target_min_y"], target_size["target_max_y"])
    res = (target_size["target_pixel_size_y"], target_size["target_pixel_size_x"])
    crs = target_size["target_crs"]
    time = ("2015-01-01", "2015-12-31")
    sensor = 'ls8'
    query =({'x': x,
            'y': y,
            'crs':crs,
            'resolution':res})

    print("Reading in data for level 3 classification")
    # ### 1. Vegetated / Non-Vegetated
    # Load data from datacube
    fc_ann = dc.load(product="fc_percentile_albers_annual",
                     measurements=["PV_PC_90", "NPV_PC_90", "BS_PC_90"],
                     time=time, **query)
    fc_ann = masking.mask_invalid_data(fc_ann)

    # Create binary layer representing vegetated (1) and non-vegetated (0)
    vegetat = ((fc_ann["PV_PC_90"] >= 50) | ((fc_ann["NPV_PC_90"] >= 50) & (fc_ann["NPV_PC_90"] <= 80)))

    # Convert to Dataset and add name
    vegetat_veg_cat_ds = vegetat.to_dataset(name="vegetat_veg_cat").squeeze().drop('time')

    # ### 2. Aquatic / Terrestrial
    # Load data from datacube
    wofs_ann = dc.load(product="wofs_annual_summary", measurements=["frequency"],
                         time=time, **query)
    wofs_ann = masking.mask_invalid_data(wofs_ann)

    item = dc.load(product="item_v2", measurements=["relative"],
                         time=time, **query)
    item = masking.mask_invalid_data(item)

    if bool(item) is True:
        item = item.squeeze().drop('time')
    else:
        print("item not available")

    mangrove = dc.load(product="mangrove_extent_cover_albers", measurements=["extent"],
                         time=time, **query)
    if bool(mangrove) is True:
        mangrove = masking.mask_invalid_data(mangrove)
    else:
        print("mangrove not available")
    
    # Create binary layer representing aquatic (1) and terrestrial (0)

    if bool(item) is True & bool(mangrove) is True:
        aquatic_wat = ((wofs_ann["frequency"] >= 0.2) | ((item["relative"] >= 2) & (item["relative"] <= 8)) | (mangrove["extent"] == 1))
    elif bool(item) is True:
        aquatic_wat = ((wofs_ann["frequency"] >= 0.2) | ((item["relative"] >= 2) & (item["relative"] <= 8)))
    elif bool(mangrove) is True:
        aquatic_wat = ((wofs_ann["frequency"] >= 0.2) | (mangrove["extent"] == 1))
    else:
        aquatic_wat = ((wofs_ann["frequency"] >= 0.2))

    # Convert to Dataset and add name
    aquatic_wat_cat_ds = aquatic_wat.to_dataset(name="aquatic_wat_cat").squeeze().drop('time')

    # ### 3. Natural Vegetation / Crop or Managed Vegetation

    # Load data from datacube
    mads = dc.load(product=sensor +"_nbart_tmad_annual", time=time, **query)
    mads = masking.mask_invalid_data(mads)

    # Normalise measurements using log function
    log_edev = numpy.log(1/mads["edev"])
    log_sdev = numpy.log(1/mads["sdev"])
    log_bcdev = numpy.log(1/mads["bcdev"])

    # Create binary layer representing cultivated (1) and natural (0)
    cultman = (((log_edev >= 1.5) & (log_edev <= 2.5)) & ((log_sdev >= 3) & (log_sdev <= 6)) & ((log_bcdev >= 1) & (log_bcdev <= 4)))

    # Convert to Dataset and add name
    cultman_agr_cat_ds = cultman.to_dataset(name="cultman_agr_cat").squeeze().drop('time')

    # ### 4. Natural Surfaces / Artificial Surfaces
    # Load data
    geomedian = dc.load(product=sensor + "_nbart_geomedian_annual", time=time, **query)
    geomedian = masking.mask_invalid_data(geomedian).squeeze().drop('time')

    # Calculate NDBI
    NDBI = ((geomedian.swir1 - geomedian.nir) / (geomedian.swir1 + geomedian.nir))

    # Create binary layer representing urban (1) and baresoil (0)
    urban = ((NDBI > -0.1) & (NDBI < 0.05)) | ((fc_ann["BS_PC_90"] >= 90) & (fc_ann["BS_PC_90"] <= 200))

    # Convert to Dataset and add name
    artific_urb_cat_ds = urban.to_dataset(name="artific_urb_cat").squeeze().drop('time')

    # ### 5. Natural Water / Artificial Water
    # Load data
    var_file = "/g/data/u46/wofs/confidence/geoFabric/geofabric.tif"
    var_name = "features"

    import_obj = gridded_ingest.LEIngestGDAL(**target_size)
    geofabric = import_obj.read_to_xarray(var_file, var_name)

    # Create binary layer representing artificial water (1) and natural water (0)
    artwatr_wat_cat = ((geofabric["features"] == 1) | (geofabric["features"] == 8))

    # Convert to Dataset and add name
    artwatr_wat_cat_ds = artwatr_wat_cat.to_dataset(name="artwatr_wat_cat")

    print("Classifying level 3")
    variables_xarray_list = []
    variables_xarray_list.append(vegetat_veg_cat_ds)
    variables_xarray_list.append(aquatic_wat_cat_ds)
    variables_xarray_list.append(cultman_agr_cat_ds)
    variables_xarray_list.append(artific_urb_cat_ds)
    variables_xarray_list.append(artwatr_wat_cat_ds)

    # Merge to a single dataframe
    classification_data = xarray.merge(variables_xarray_list)

    # Apply Level 3 classification using separate function. Works through in three stages
    level1, level2, level3 = lccs_l3.classify_lccs_level3(classification_data)

    # Save classification values back to xarray
    out_class_xarray = xarray.Dataset(
        {"level1" : (classification_data["vegetat_veg_cat"].dims, level1),
         "level2" : (classification_data["vegetat_veg_cat"].dims, level2),
         "level3" : (classification_data["vegetat_veg_cat"].dims, level3)})
    classification_data = xarray.merge([classification_data, out_class_xarray])

    # Convert level3 to Dataset and add name
    level3_ds = classification_data.level3.to_dataset(name="level3")

    print("Reading in data for level 4 classification")
    waterstt_wat_cat_ds = aquatic_wat.to_dataset(name="waterstt_wat_cat").squeeze().drop('time')

    # Load data from datacube
    waterper = wofs_ann.frequency.squeeze().drop('time')

    '''
    WOfs does not provide hydroperiod in days, best alternative is frequency (wet/dry obs)
    which have been estimated based on months/year

    0.74 - 1    --> 1 : ("B1", "Perennial (> 9 months)"),
    0.58 - 0.74 --> 7 : ("B7", "Non-perennial (7 to 9 months)")
    0.3 - 0.58  --> 8 : ("B8", "Non-perennial (4 to 7 months)")
    0 - 0.3     --> 9 : ("B9", "Non-perennial (1 to 4 months)")

    '''
    waterper2 = numpy.zeros_like(waterper, dtype='float64')
    waterper2 = numpy.where((waterper > 0) & (waterper < 1), waterper*12*30, waterper2)
    
    waterper_reclass = xarray.DataArray(waterper2,
                                coords={'y': waterper['y'].values,
                                        'x': waterper['x'].values},
                                    dims=['y', 'x'])
    
    # Convert to Dataset and add name
    waterper_wat_cin_ds = waterper_reclass.to_dataset(name="waterper_wat_cin")

    # ### 3. Lifeform
    # Create Dataset
    lifeform = copy.deepcopy(fc_ann["PV_PC_90"])
    lifeform.values = numpy.zeros_like(fc_ann["PV_PC_90"], dtype='float64')
    lifeform.values = numpy.where((fc_ann["PV_PC_90"] < 40.) & (fc_ann["PV_PC_90"] >= 0.), 2, lifeform.values)
    lifeform.values = numpy.where((fc_ann["PV_PC_90"] >= 40.) & (fc_ann["PV_PC_90"] <= 100), 1, lifeform.values)

    # Convert to Dataset and add name
    lifeform_veg_cat_ds = lifeform.to_dataset(name="lifeform_veg_cat").squeeze().drop('time')

    # ### 4. Canopy Cover
    # Create Dataset
    canopycover = copy.deepcopy(fc_ann["PV_PC_90"])
    canopycover.values = numpy.zeros_like(fc_ann["PV_PC_90"], dtype='float64')
    canopycover.values = numpy.where((fc_ann["PV_PC_90"] <= 100.) & (fc_ann["PV_PC_90"] >= 1.), fc_ann["PV_PC_90"], canopycover.values)

    # Convert to Dataset and add name
    canopyco_veg_con_ds = canopycover.to_dataset(name="canopyco_veg_con").squeeze().drop('time')


    ## Level 4 Classification ##
    print("Applying level 4 classification")
    variables_xarray_list = []
    variables_xarray_list.append(level3_ds)
    variables_xarray_list.append(lifeform_veg_cat_ds)
    variables_xarray_list.append(canopyco_veg_con_ds)
    variables_xarray_list.append(waterper_wat_cin_ds)
    variables_xarray_list.append(waterstt_wat_cat_ds)

    # Merge to a single dataframe
    l4_classification_data = xarray.merge(variables_xarray_list)

    # Apply Level 4 classification
    classification_array = lccs_l4.classify_lccs_level4(l4_classification_data)

    # Get Level 4 codes to apply colour scheme
    l4_classification_codes = lccs_l4.get_lccs_level4_code(classification_array)

    # To check the results for level 3 use colour_lccs_level3 to get the colour scheme.
    red, green, blue, alpha = lccs_l4.colour_lccs_level4(l4_classification_codes)
    # WIP: can't get plot to populate, problem with colour scheme maybe?
    #pyplot.figure(figsize=(12, 10))
    #pyplot.imshow(numpy.dstack([red, green, blue, alpha]))

    # ### **Save results to geotiff**
    print("Saving to GeoTiff")
    output_l4_rgb_file_name = config["output_l4_rgb_tiff"]
    out_file_transform = [config["resolution"][0], 0, config["extent"]["min_x"],
                          0, config["resolution"][1], config["extent"]["max_y"]]

    output_x_size = int((config["extent"]["max_x"] - config["extent"]["min_x"]) / config["resolution"][0])
    output_y_size = int((config["extent"]["min_y"] - config["extent"]["max_y"]) / config["resolution"][1])

    rgb_dataset = rasterio.open(output_l4_rgb_file_name, 'w', driver='GTiff',
                                    height=output_y_size, width=output_x_size,
                                    count=3, dtype=level3.dtype,
                                    crs=config["crs"], transform=out_file_transform)
    rgb_dataset.write(red, 1)
    rgb_dataset.write(green, 2)
    rgb_dataset.write(blue, 3)
    rgb_dataset.close()

if __name__ == "__main__":

    # Read in config file
    parser = argparse.ArgumentParser(description="Run LE LCCS Level 4 Classification "
                                                 "on ODC.")
    parser.add_argument("configfile",
                        type=str,
                        nargs=1,
                        help="Config file")
    args = parser.parse_args()
    run_l4_classification(args.configfile[0])
