# change calculation script
import os
import glob
import sys

# Pass path to LCCS module
sys.path.append("/g/data/u46/users/sc0554/LCCS/livingearth_lccs/le_lccs/")

from le_lccs.le_utils import change_utils

# Specify directories for each year of processing and version for file name
year_1 = "2010"
year_2 = "2015"
version = "v-0.5.0.tif"
path_1 = "/g/data/r78/LCCS_Aberystwyth/continental_run_april2020/2010/values/"
path_2 = "/g/data/r78/LCCS_Aberystwyth/continental_run_april2020/2015/values/"

# Create list of files to be processed
files_1 = [f for f in glob.glob(path_1 + "*L4*.tif", recursive=False)]
files_2 = [f for f in glob.glob(path_2 + "*L4*.tif", recursive=False)]

# Specify output directory
output_dir = "/g/data/r78/LCCS_Aberystwyth/change_tests"

# Pass file with tiles to process 
tile_file = open('tile_list.txt', 'r')
tiles = tile_file.read().splitlines()
tile_file.close()

for tile in tiles:
    print("Processing tile", tile)
    # Find matching files for tile, append "_" to prevent incorrect matches
    tile_index_1 = [i for i, s in enumerate(files_1) if "_"+tile in s]
    tile_index_2 = [i for i, s in enumerate(files_2) if "_"+tile in s]
    f_1 = files_1[tile_index_1[0]] 
    f_2 = files_2[tile_index_2[0]]
    # Create file output names
    out_level_3_change_file = os.path.join(output_dir, f"{year_1}_{year_2}_{tile}_L3C_{version}")
    out_level_4_change_file = os.path.join(output_dir, f"{year_1}_{year_2}_{tile}_L4C_{version}")
    out_ebc_file = os.path.join(output_dir, f"{year_1}_{year_2}_{tile}_EBC_{version}")
    # Calculate change and write out files
    change_utils.calc_change_gtif(f_1, f_2, out_level3_change_file=out_level_3_change_file, out_level4_change_file=out_level_4_change_file, out_ebc_file=out_ebc_file)
