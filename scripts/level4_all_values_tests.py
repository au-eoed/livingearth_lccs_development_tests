#!/usr/bin/env python
"""
Script to calculate all possible inputs for LCCS Level 4 classification and
use these to calculate all possible Level 4 codes and descriptions.

Dan Clewley - 2019-05-17

"""
import argparse
import numpy
import xarray

import sys
sys.path.append("../../livingearth_lccs")

from le_lccs.le_classification import lccs_l4

# Print output variable names
PRINT_OUTPUT = True


def write_dict_to_layer_text_files(values_dict_list, layer_text_files_dict):
    """
    Write dictionary to a separate text file for each key
    """
    for values_dict in values_dict_list:
        for layer_name, value in values_dict.items():
            layer_text_files_dict[layer_name].write("{}\n".format(value))

def write_class_description_to_dict(values_dict, all_class_codes_descriptions={},
                                    in_classification_data=None):
    """
    Calculate level 4 class for values dictionary
    """
    if in_classification_data is None:
        dataset_dict = {}
        for layer_name, value in values_dict[0].items():
            dataset_dict[layer_name] = ("object", numpy.array([value]))
        in_classification_data = xarray.Dataset(dataset_dict)
    else:
        for layer_name, value in values_dict[0].items():
            in_classification_data[layer_name].values[0] = value

    classification_array = lccs_l4.classify_lccs_level4(in_classification_data)
    class_code = lccs_l4.get_lccs_level4_code(classification_array)
    class_description = lccs_l4.get_lccs_level4_description(classification_array)

    if class_code is None or class_description is None:
        return None

    # If class code isn't already in array print it to the screen
    try:
        all_class_codes_descriptions[class_code[0]]
    except KeyError:
        if PRINT_OUTPUT:
            print("{} : {}".format(class_code[0], class_description[0]))

    # Save out to dictionary
    all_class_codes_descriptions[class_code[0]] = class_description[0]

    return in_classification_data

def create_var_list(in_vals_lsts, val_dict=None, layer_text_files_dict=None,
                  all_class_codes_descriptions={},
                  in_classification_data=None):
    """
    A function which will produce a list of dictionaries with all the combinations 
    of the input variables listed (i.e., the powerset).

    Taken from RSGISLib and modified to write out data to text files or
    calculate lccs_level4 classes.

    in_vals_lsts - dictionary with each value having a list of values.
    val_dict - variable used in iterative nature of function which lists
               the variable for which are still to be looped through. Would 
               normally not be provided by the user as default is None. Be
               careful if you set as otherwise.
    returns: list of dictionaries with the same keys are the input but only a
             single value will be associate with key rather than a list.

    Example::

    seg_vars_ranges = dict()
    seg_vars_ranges['k'] = [5, 10, 20, 30, 40, 50, 60, 80, 100, 120]
    seg_vars_ranges['d'] = [10, 20, 50, 100, 200, 1000, 10000]
    seg_vars_ranges['minsize'] = [5, 10, 20, 50, 100, 200]

    seg_vars = rsgis_utils.create_var_list(seg_vars_ranges)

    """
    out_vars = []
    if (in_vals_lsts is None) and (val_dict is not None):
        out_val_dict = dict()
        for key in val_dict.keys():
            out_val_dict[key] = val_dict[key]
        out_vars.append(out_val_dict)
    elif in_vals_lsts is not None:
        if len(in_vals_lsts.keys()) > 0:
            key = list(in_vals_lsts.keys())[0]
            vals_arr = in_vals_lsts[key]
            next_vals_lsts = dict()
            for ckey in in_vals_lsts.keys():
                if ckey != key:
                    next_vals_lsts[ckey] = in_vals_lsts[ckey]

            if len(next_vals_lsts.keys()) == 0:
                next_vals_lsts = None

            if val_dict is None:
                val_dict = dict()

            for val in vals_arr:
                c_val_dict = dict()
                for ckey in val_dict.keys():
                    c_val_dict[ckey] = val_dict[ckey]
                c_val_dict[key] = val
                c_out_vars = create_var_list(next_vals_lsts, c_val_dict,
                                             layer_text_files_dict,
                                             all_class_codes_descriptions=all_class_codes_descriptions,
                                             in_classification_data = in_classification_data)
                if len(c_out_vars) > 0 and layer_text_files_dict is not None:
                    write_dict_to_layer_text_files(c_out_vars, layer_text_files_dict)
                if len(c_out_vars) > 0:
                    in_classification_data = write_class_description_to_dict(
                        c_out_vars,
                        all_class_codes_descriptions=all_class_codes_descriptions,
                        in_classification_data=in_classification_data)

    return out_vars

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("outputcsv", nargs=1, type=str,
                        help="Output CSV file to save to")
    args = parser.parse_args()

    out_dict_file = args.outputcsv[0]

    # Set up a dictionary of each variable to pass to create_var_list
    input_layer_vars_dict = {}

    try:
        for layer_class in lccs_l4.ALL_L4_LAYER_CLASSES[0:5]:
            # For all classes except level3 also store 0 (no data).
            if layer_class().input_layer_name != "level3":
                input_layer_vars_dict[layer_class().input_layer_name] = [numpy.nan] + layer_class().get_valid_input_values()
            else:
                input_layer_vars_dict[layer_class().input_layer_name] = layer_class().get_valid_input_values()

        # Calculate class code for all input variables and save to a dictionary
        all_class_codes_descriptions = {}
        create_var_list(input_layer_vars_dict, layer_text_files_dict=None,
                        all_class_codes_descriptions=all_class_codes_descriptions)
        print("All classes calculated")
    except KeyboardInterrupt:
        print("Interrupt - saving classes calculated")
    finally:
        # Write out dictionary to text file.
        out_f = open(out_dict_file, "w")
        for code, description in all_class_codes_descriptions.items():
            out_f.write('{},"{}"\n'.format(code, description))
        out_f.close()

    print("Saved to: {}".format(out_dict_file))
