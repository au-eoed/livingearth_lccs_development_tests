import rasterio, xarray, numpy as np, matplotlib.pyplot as plt
import scipy.signal, scipy.ndimage, rasterio.windows
from datacube.utils.cog import write_cog
sourcename = "/g/data/r78/LCCS_Aberystwyth/ls8_mad.vrt"
f = rasterio.open(sourcename, mode='r')
print(f.count, f.shape)
ds = xarray.open_rasterio(sourcename, chunks=dict(x=10000, y=10000))
# Band 1 = sdev, band 2 = edev
spectral = ds.isel(band=0)#.sel(x=slice(1800000, 1900000), y=slice(-3000000, -3200000))
spectral.data = spectral.data.map_overlap(scipy.ndimage.gaussian_filter, depth=100, sigma=1, dtype=np.float32).compute()
#spectral.data = spectral.data * 100000000
#spectral.data = spectral.data.astype(np.int16)
write_cog(spectral, 'ls8_mad_smoothed_30_sdev.tif', overwrite=True, BIGTIFF=True)
