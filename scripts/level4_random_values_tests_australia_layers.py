#!/usr/bin/env python
"""
Modified script to calculate all possible classes for Australia given agreed layers used for Level 4

Dan Clewley - 2019-11-15

Run using:

./level4_random_values_tests_australia_layers.py level4_classes_australia.csv lccs_l4_colour_scheme.csv

Was designed for a lot more layers so randomly selects then breaks when getting no new classes.

"""
import argparse
import csv
import logging
import sys

import numpy
import xarray

numpy.warnings.filterwarnings('ignore')

sys.path.append("../../livingearth_lccs")

from le_lccs.le_classification import lccs_l4
from le_lccs.le_classification import l4_layers_lccs
from le_lccs.le_classification import l4_layers_AU_modified

# Print output variable names
PRINT_OUTPUT = False

# Number of items to pick for each later (i.e., number to calculate at once).
NUM_PICKS = 10000

# Maximum number of runs
# Total number of random combinations will be NUM_PICKS x MAX_RUNS.
MAX_RUNS = 10000

# Set up dictionary of layers for Australia. Different from standard LCCS list.
AUSTRALIA_L4_LAYER_CLASSES = [l4_layers_lccs.LCCSLevel3,
                              l4_layers_lccs.LifeformVegCatL4a,
                              l4_layers_lccs.CanopycoVegCatL4d,
                              # l4_layers_AU_modified.CanopycoVegCatL4d_AU,
                              # l4_layers_lccs.CanopyhtVegCatL4d,
                              # l4_layers_lccs.WaterseaVegCatL4a,
                              l4_layers_AU_modified.WaterseaVegCatL4a_AU,
                              l4_layers_lccs.WatersttWatCatL4a,
                              l4_layers_lccs.WaterperWatCatL4d,
                              l4_layers_lccs.InttidalWatCatL4a,
                              l4_layers_AU_modified.BaregradPhyCatL4d_AU]


def write_class_description_to_dict(values_dict, all_class_codes_descriptions={},
                                    out_colours_csv_list=[],
                                    in_classification_data=None,
                                    num_picks=NUM_PICKS):
    """
    Calculate level 4 class for values dictionary
    """
    if in_classification_data is None:
        dataset_dict = {}
        for layer_name, possible_values in values_dict.items():
            dataset_dict[layer_name] = ("object",
                                        numpy.random.choice(possible_values, num_picks))
        in_classification_data = xarray.Dataset(dataset_dict,
                                                coords={"object" : numpy.arange(0, num_picks)})
    else:
        for layer_name, possible_values in values_dict.items():
            in_classification_data[layer_name].values = \
                numpy.random.choice(possible_values, num_picks)

    classification_array = lccs_l4.classify_lccs_level4(in_classification_data, 
                                                        AUSTRALIA_L4_LAYER_CLASSES)
    class_codes = lccs_l4.get_lccs_level4_code(classification_array,
                                               AUSTRALIA_L4_LAYER_CLASSES)
    class_descriptions = lccs_l4.get_lccs_level4_description(classification_array,
                                                             AUSTRALIA_L4_LAYER_CLASSES)
    colours_red, colours_green, colours_blue, colours_alpha = lccs_l4.colour_lccs_level4(classification_array)

    # If class code isn't already in array print it to the screen
    new_classes = 0
    for row in range(classification_array["level3"].size):
        class_code = str(class_codes[row])
        class_description = str(class_descriptions[row])
        try:
            all_class_codes_descriptions[class_code]
        except KeyError:
            # Save out to dictionary
            all_class_codes_descriptions[class_code] = class_description
            # Make up line to save out to CSV file (dict writer)
            out_col_csv_line = {}

            for var in classification_array.data_vars:
                try:
                    var_val = int(classification_array[var][row])
                    if var_val == 0:
                        var_val = ""
                except ValueError:
                    out_col_csv_line[var] = ""
                out_col_csv_line[var] = var_val

            out_col_csv_line["LCCS_Code"] = class_code
            out_col_csv_line["LCCS_Description"] = class_description
            out_col_csv_line["Red"] = colours_red[row]
            out_col_csv_line["Green"] = colours_green[row]
            out_col_csv_line["Blue"] = colours_blue[row]
            out_col_csv_line["Alpha"] = colours_alpha[row]
            out_colours_csv_list.append(out_col_csv_line)

            new_classes += 1
            if PRINT_OUTPUT:
                print("{} : {}".format(class_code, class_description))

    if new_classes == 0:
        raise StopIteration("No new classes found")

    return in_classification_data

if __name__ == "__main__":

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("outputcsv", nargs=1, type=str,
                        help="Output CSV file to save to")
    parser.add_argument("outputcolscsv", nargs=1, type=str,
                        help="Output CSV file to save to colour scheme template to")
    parser.add_argument("--max_runs", type=int, default=MAX_RUNS,
                        help="Maximum number of runs")
    parser.add_argument("--num_picks", type=int, default=NUM_PICKS,
                        help="Number of picks per run")
    args = parser.parse_args()

    # Get output files to write to
    out_dict_file = args.outputcsv[0]
    out_col_csv_file = args.outputcolscsv[0]

    # Set up logger
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

    # Set up a dictionary of each variable to pass to create_var_list
    input_layer_vars_dict = {}
    all_class_codes_descriptions = {}
    out_colours_csv_list = []

    total_outputs = 1
    try:
        for layer_class in AUSTRALIA_L4_LAYER_CLASSES:
            # For all classes except level3 also store 0 (no data).
            if layer_class().input_layer_name == "lifeform_veg_cat":
                input_layer_vars_dict[layer_class().input_layer_name] = numpy.array([numpy.nan] + [1, 2])
                total_outputs = total_outputs * input_layer_vars_dict[layer_class().input_layer_name].size
            elif layer_class().input_layer_name == "waterstt_wat_cat":
                input_layer_vars_dict[layer_class().input_layer_name] = numpy.array([numpy.nan] + [1, 2])
                total_outputs = total_outputs * input_layer_vars_dict[layer_class().input_layer_name].size
            elif layer_class().input_layer_name == "watersea_veg_cat":
                input_layer_vars_dict[layer_class().input_layer_name] = numpy.array([numpy.nan] + [1, 2])
                total_outputs = total_outputs * input_layer_vars_dict[layer_class().input_layer_name].size
            elif layer_class().input_layer_name == "inttidal_wat_cat":
                input_layer_vars_dict[layer_class().input_layer_name] = numpy.array([0, 3])
                total_outputs = total_outputs * input_layer_vars_dict[layer_class().input_layer_name].size
            elif layer_class().input_layer_name == "canopyco_veg_con":
                print(layer_class().output_codes_descriptions)
                input_layer_vars_dict[layer_class().input_layer_name] = numpy.array([numpy.nan] + layer_class().get_valid_input_values())
                total_outputs = total_outputs * input_layer_vars_dict[layer_class().input_layer_name].size
            elif layer_class().input_layer_name != "level3":
                print(layer_class().output_codes_descriptions)
                input_layer_vars_dict[layer_class().input_layer_name] = numpy.array(layer_class().get_valid_input_values())
                total_outputs = total_outputs * input_layer_vars_dict[layer_class().input_layer_name].size
            else:
                input_layer_vars_dict[layer_class().input_layer_name] = numpy.array([111, 112, 123, 124, 215, 216, 220])
                total_outputs = total_outputs * input_layer_vars_dict[layer_class().input_layer_name].size

        print(input_layer_vars_dict)

        print("Total of {} layers".format(len(lccs_l4.ALL_L4_LAYER_CLASSES)))
        print("Maximum number of input combinations is {:,}".format(total_outputs))
        print("Testing with {:,} random combinations".format(args.max_runs * args.num_picks))
        # Calculate class code for all input variables and save to a dictionary
        in_classification_data = None

        for i in range(1, args.max_runs+1):
            print("Run {}/{}".format(i, args.max_runs))
            try:
                in_classification_data = \
                    write_class_description_to_dict(input_layer_vars_dict,
                                                    all_class_codes_descriptions,
                                                    out_colours_csv_list,
                                                    in_classification_data,
                                                    num_picks=args.num_picks)
            except StopIteration as e:
                print(e)
                break
    except KeyboardInterrupt:
        print("Interrupt - saving classes calculated")
    finally:
        # Write out dictionary to text file.
        out_f = open(out_dict_file, "w")
        all_class_codes = list(all_class_codes_descriptions.keys())
        all_class_codes.sort()
        out_f.write('Code,Description\n')
        for code in all_class_codes:
            out_f.write('{},"{}"\n'.format(code, all_class_codes_descriptions[code]))
        out_f.close()

        # Write out colour scheme CSV file template
        out_cf = open(out_col_csv_file, "w")
        fieldnames = list(out_colours_csv_list[0].keys())
        writer = csv.DictWriter(out_cf, fieldnames=fieldnames)
        writer.writeheader()

        for line in out_colours_csv_list:
            writer.writerow(line)
        out_cf.close()
    print("Saved to: {}".format(out_dict_file))
