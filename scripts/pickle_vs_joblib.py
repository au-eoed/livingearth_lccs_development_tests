from time import time
import pickle
import os
import joblib
path = "/g/data/r78/LCCS_Aberystwyth/training_data/cultivated/2010_2015_training_data_combined_24072020"
file_1 = os.path.join(path, "2010_2015_median_model_indices_feature_selection_kbest_15.pickle")
t1 = time()
a = pickle.load(open(file_1, "rb"))
print("pickle", time()-t1)
file_2 = os.path.join(path, "2010_2015_median_model_indices_feature_selection_kbest_15.joblib")
t1 = time()
b = joblib.load(file_2)
print("joblib", time()-t1)
